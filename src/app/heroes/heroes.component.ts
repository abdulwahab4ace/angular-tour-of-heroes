import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';
import { List } from 'immutable';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  public heroes: Hero[];
  // public heroes: List<Hero[]>;
  
  constructor(private heroService: HeroService) { }

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.heroService.getHeroes()
        .subscribe(heroes => this.heroes = heroes.heroes);
  }

  add(name: string): void {
    name = name.trim();
    if(!name) { return; }
    this.heroService.addHero({ name } as Hero)
        .subscribe((responce) => {
          if(responce.added){
            this.heroes.push(responce.hero);
          }else{
            console.log("Someting is wrong while adding hero");
          }
        });
  }

  delete(hero: Hero): void{
    this.heroService.deleteHero(hero.id).subscribe(
      (responce) => {
        if(responce.deleted){
          this.heroes = this.heroes.filter(h => h != hero);
        }else{
          console.log("Someting is wrong while deleting hero");
        }
      });
  }
}
