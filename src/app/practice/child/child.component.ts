import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Hero } from '../../hero';


@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {

  @Input() heroes: Hero[];
  @Output() deleteHero: EventEmitter<Hero> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  delete(hero: Hero){
    this.deleteHero.emit(hero);
  }

}
