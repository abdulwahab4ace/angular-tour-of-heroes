import { Component, OnInit, DoCheck} from '@angular/core';
import { HeroService } from '../hero.service';
import { Hero } from '../hero';

@Component({
  selector: 'app-practice',
  templateUrl: './practice.component.html',
  styleUrls: ['./practice.component.css']
})
export class PracticeComponent implements OnInit, DoCheck {

  public heroes: Hero[];
  public msg: string;

  constructor(
    public heroService: HeroService
  ) { }

  ngOnInit() {
    this.getHeroes();
  }

  ngDoCheck(){
    if(this.msg){
      console.log('this.msg = ',this.msg);
    }
  }

  getHeroes() : void {
    this.heroService.getHeroes()
        .subscribe(heroes => this.heroes = heroes.heroes);
  }

  deleteHero(event: any){
    this.heroService.deleteHero(event.id)
        .subscribe(res => this.msg = res);

    var index = this.heroes.findIndex(x => x.id == event.id);
    if (index > -1) {
      this.heroes.splice(index, 1);
    }
  }

}
