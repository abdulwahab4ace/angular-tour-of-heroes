import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientXsrfModule } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { MessageService } from './message.service';
import { Hero } from './hero';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})

export class HeroService {
  
  private heroesUrl = 'api/heroes'; //URL to web api

  private laravelUrl = "http://localhost:8000";

  constructor(
    private messageService: MessageService,
    private http: HttpClient
  ) { }
  
  private log(message: string){
    this.messageService.add(`HeroService: ${message}`);
  }

  /**
   * getHeroes() by Http request
   */
  getHeroes(): Observable<any> {
    return this.http.get<any>(`${this.laravelUrl}/json`)
      .pipe(
        tap(heroes => this.log('fetched heroes')),
        catchError(this.handleError('getHeroes',[]))
      );
  }

  /**
   * @param id - id of hero
   * get hero by id : Http request
   */
  getHero(id: number): Observable<any> {
    const url = `${this.laravelUrl}/json/${id}`;
    return this.http.get<any>(url)
      .pipe(
        tap(_ => this.log(`fetched hero id=${id}`)),
        catchError(this.handleError<Hero>(`getHero id=${id}`))
      );
  } 

  /**
   * @param hero
   * PUT: update the hero on the server ny http request(PUT)
   */
  updateHero (hero: Hero): Observable<any> {
    return this.http.put(`${this.laravelUrl}/json/update`, hero, httpOptions)
      .pipe(
        tap(_ => this.log(`updated hero id=${hero.id}`)),
        catchError(this.handleError<any>('updatehero'))
      );
  }

  /**
   *  POST: add a new hero to the server by http POST request
   * 
   */
  addHero (hero: Hero): Observable<any> {
    var name: string = hero.name;
    return this.http.post<Hero>(`${this.laravelUrl}/json/post/add`, hero, httpOptions)
      .pipe(
        tap((hero: Hero) => this.log(`added hero having name=${name}`)),
        catchError(this.handleError<Hero>('addHero'))
      );
  }

  /**
   * delete hero: by http request(delete)
   * @param Hero
   */
  deleteHero(heroId: number): Observable<any>{
    const url = `${this.laravelUrl}/json/delete/${heroId}`;

    return this.http.delete<string>(url)
      .pipe(
        tap(_=>this.log(`deleted hero id=${heroId}`)),
        catchError(this.handleError<string>('deleteHero'))
      );
  }

  /* GET heroes whose name contains search term */
  searchHeroes(term: string): Observable<Hero[]> {
    if (!term.trim()) {
      // if not search term, return empty hero array.
      return of([]);
    }
    return this.http.get<Hero[]>(`${this.laravelUrl}/json/search/${term}`).pipe(
      tap(_ => this.log(`found heroes matching "${term}"`)),
      catchError(this.handleError<Hero[]>('searchHeroes', []))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}